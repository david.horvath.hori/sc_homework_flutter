import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';
import 'package:sc_homework/features/saved_beers/presentation/bloc/saved_beers_bloc.dart';
import 'package:sc_homework/features/saved_beers/presentation/bloc/saved_beers_event.dart';
import 'package:sc_homework/features/saved_beers/presentation/bloc/saved_beers_state.dart';
import 'package:sc_homework/features/saved_beers/presentation/widget/beer_list_tile.dart';
import 'package:sc_homework/features/saved_beers/presentation/widget/empty_view.dart';
import 'package:sc_homework/injection_container.dart';

class SavedBeersPage extends StatelessWidget {
  const SavedBeersPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SavedBeersBloc>(
      create: (context) => sl()..add(const SavedBeersEventFetch()),
      child: const SavedBeersView(),
    );
  }
}

class SavedBeersView extends StatelessWidget {
  const SavedBeersView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Your Saved Beers"),
        actions: [
          IconButton(
            icon: const Icon(Icons.delete_forever),
            tooltip: 'Clear',
            onPressed: () {
              context.read<SavedBeersBloc>().add(const SavedBeersEventDelete());
            },
          ),
        ],
      ),
      body: BlocBuilder<SavedBeersBloc, SavedBeersState>(
        builder: (context, state) {
          if (state is SavedBeerStateLoading) {
            return Center(
                child: LoadingAnimationWidget.newtonCradle(
              color: Colors.blueAccent,
              size: 100,
            ));
          } else if (state is SavedBeerStateDone) {
            return _buildRepositoryList(context, state.beers ?? []);
          }
          return Container();
        },
      ),
    );
  }

  Widget _buildRepositoryList(BuildContext context, List<BeerEntity> beers) {
    if (beers.isEmpty) {
      return const EmptyView();
    }
    return ListView.builder(
      itemCount: beers.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {},
          child: BeerListItem(beer: beers[index]),
        );
      },
    );
  }
}
