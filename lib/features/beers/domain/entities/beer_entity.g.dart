// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: implicit_dynamic_parameter

part of 'beer_entity.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BeerEntityAdapter extends TypeAdapter<BeerEntity> {
  @override
  final int typeId = 1;

  @override
  BeerEntity read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BeerEntity(
      id: fields[0] as int?,
      tagline: fields[1] as String?,
      description: fields[2] as String?,
      imageUrl: fields[3] as String?,
      brewersTips: fields[4] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, BeerEntity obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.tagline)
      ..writeByte(2)
      ..write(obj.description)
      ..writeByte(3)
      ..write(obj.imageUrl)
      ..writeByte(4)
      ..write(obj.brewersTips);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BeerEntityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
