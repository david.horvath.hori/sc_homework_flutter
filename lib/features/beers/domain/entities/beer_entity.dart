import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:sc_homework/features/beers/data/models/beer.dart';

part 'beer_entity.g.dart';

@HiveType(typeId: 1)
class BeerEntity extends Equatable {
  @HiveField(0)
  final int? id;
  final String? name;
  @HiveField(1)
  final String? tagline;
  @HiveField(2)
  final String? description;
  @HiveField(3)
  final String? imageUrl;
  @HiveField(4)
  final String? brewersTips;

  const BeerEntity({
    this.id,
    this.name,
    this.tagline,
    this.description,
    this.imageUrl,
    this.brewersTips,
  });

  factory BeerEntity.fromModel(Beer model) {
    return BeerEntity(
        id: model.id,
        name: model.name,
        tagline: model.tagline,
        description: model.description,
        imageUrl: model.imageUrl,
        brewersTips: model.brewersTips);
  }

  @override
  List<Object?> get props => [
        id,
        name,
        tagline,
        description,
        imageUrl,
        brewersTips,
      ];
}
