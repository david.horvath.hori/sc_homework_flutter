import 'package:equatable/equatable.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';

enum SavedBeerEventStatus { initial, loading, success, empty, error }

abstract class SavedBeersState extends Equatable {
  final List<BeerEntity>? beers;

  const SavedBeersState({this.beers});

  @override
  List<Object> get props => [beers!];
}

class SavedBeerStateLoading extends SavedBeersState {
  const SavedBeerStateLoading();
}

class SavedBeerStateDone extends SavedBeersState {
  final SavedBeerEventStatus status;

  const SavedBeerStateDone(
      {required this.status, required List<BeerEntity> beers})
      : super(beers: beers);
}
