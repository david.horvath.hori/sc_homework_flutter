import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_homework/features/beer_details/domain/usecases/get_saved_beer_usecase.dart';
import 'package:sc_homework/features/beer_details/presentation/bloc/beer_details_event.dart';
import 'package:sc_homework/features/beer_details/presentation/bloc/beer_details_state.dart';

class BeerDetailsBloc extends Bloc<BeerDetailsEvent, BeerDetailsState> {
  final GetSavedBeerUseCase _getSavedBeerUseCase;

  BeerDetailsBloc(
    this._getSavedBeerUseCase,
  ) : super(const BeerDetailsStateLoading()) {
    on<BeerDetailsEventInitial>(_onInitial);
  }

  void _onInitial(
      BeerDetailsEventInitial event, Emitter<BeerDetailsState> emit) async {
    var beer = await _getSavedBeerUseCase.call(params: event.beerId);
    emit(BeerDetailsStateDone(beer: beer));
  }
}
