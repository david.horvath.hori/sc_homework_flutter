import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';

class AppConstants {
  static const mockedBeer = BeerEntity(
    id: 1,
    name: "Punk IPA 2007 - 2010 (Mocked)",
    tagline: "Post Modern Classic. Spiky. Tropical. Hoppy.",
    description:
        "Our flagship beer that kick started the craft beer revolution. This is James and Martin's original take on an American IPA, subverted with punchy New Zealand hops. Layered with new world hops to create an all-out riot of grapefruit, pineapple and lychee before a spiky, mouth-puckering bitter finish.",
    imageUrl: "https://images.punkapi.com/v2/192.png",
    brewersTips:
        "While it may surprise you, this version of Punk IPA isn't dry hopped but still packs a punch! To make the best of the aroma hops make sure they are fully submerged and add them just before knock out for an intense hop hit.",
  );

  static const String featureBoxName = 'features';
}
