import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:sc_homework/features/beer_details/presentation/bloc/beer_details_bloc.dart';
import 'package:sc_homework/features/beer_details/presentation/bloc/beer_details_event.dart';
import 'package:sc_homework/features/beer_details/presentation/bloc/beer_details_state.dart';
import 'package:sc_homework/features/beer_details/presentation/widget/beer_details_view.dart';
import 'package:sc_homework/injection_container.dart';

class BeerDetailsPage extends StatelessWidget {
  final int beerId;
  const BeerDetailsPage({required this.beerId, super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BeerDetailsBloc>(
      create: (context) => sl()..add(BeerDetailsEventInitial(beerId: beerId)),
      child: const BeerDetailsView(),
    );
  }
}

class BeerDetailsView extends StatelessWidget {
  const BeerDetailsView({super.key});

  Widget _buildBody() {
    return BlocBuilder<BeerDetailsBloc, BeerDetailsState>(
      builder: (context, state) {
        if (state is BeerDetailsStateLoading) {
          return Center(
              child: LoadingAnimationWidget.newtonCradle(
            color: Colors.blueAccent,
            size: 100,
          ));
        } else if (state is BeerDetailsStateDone) {
          return BeerDetails(
            beer: state.beer!,
          );
        }
        return Container();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Beer Details"),
      ),
      body: _buildBody(),
    );
  }
}
