import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:sc_homework/features/beers/presentation/bloc/beers_bloc.dart';
import 'package:sc_homework/features/beers/presentation/bloc/beers_event.dart';
import 'package:sc_homework/features/beers/presentation/bloc/beers_state.dart';
import 'package:sc_homework/features/beers/presentation/widget/beer_card.dart';
import 'package:sc_homework/features/saved_beers/presentation/page/saved_beers_page.dart';
import 'package:sc_homework/injection_container.dart';

class BeersPage extends StatelessWidget {
  const BeersPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BeersBlock>(
      create: (context) => sl()..add(const BeersEventFetch(counter: 0)),
      child: const BeersView(),
    );
  }
}

class BeersView extends StatelessWidget {
  const BeersView({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<BeersBlock, BeersState>(
      listener: (context, state) {
        if (state.counter != null && state.counter! > 10) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => const SavedBeersPage(),
            ),
          );
          var _beersBloc = context.read<BeersBlock>();
          _beersBloc.add(const BeersEventFetch(counter: 0));
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Beers"),
          actions: [
            IconButton(
              icon: const Icon(Icons.list),
              tooltip: 'Your Saved Beers',
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const SavedBeersPage(),
                  ),
                );
              },
            ),
          ],
        ),
        body: BlocBuilder<BeersBlock, BeersState>(
          builder: (context, state) {
            if (state is BeersStateLoading) {
              return Center(
                  child: LoadingAnimationWidget.newtonCradle(
                color: Colors.blueAccent,
                size: 100,
              ));
            } else if (state is BeersStateDone) {
              return BeerCard(
                beer: state.beer,
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
