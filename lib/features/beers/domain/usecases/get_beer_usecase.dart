import 'package:sc_homework/core/usecase/usecase.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';
import 'package:sc_homework/features/beers/domain/repositories/beer_repository.dart';

class GetBeerUseCase implements UseCase<BeerEntity, void> {
  final BeerRepository _beerRepository;

  GetBeerUseCase(this._beerRepository);

  @override
  Future<BeerEntity> call({void params}) async {
    return _beerRepository.getRandomBeer();
  }
}
