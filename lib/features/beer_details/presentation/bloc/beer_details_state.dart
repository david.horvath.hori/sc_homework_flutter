import 'package:equatable/equatable.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';

abstract class BeerDetailsState extends Equatable {
  final BeerEntity? beer;

  const BeerDetailsState({this.beer});

  @override
  List<Object> get props => [beer!];
}

class BeerDetailsStateLoading extends BeerDetailsState {
  const BeerDetailsStateLoading();
}

class BeerDetailsStateDone extends BeerDetailsState {
  const BeerDetailsStateDone({required BeerEntity beer}) : super(beer: beer);
}
