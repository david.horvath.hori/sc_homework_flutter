import 'package:json_annotation/json_annotation.dart';

part 'beer.g.dart';

@JsonSerializable()
class Beer {
  final int? id;
  final String? name;
  final String? tagline;
  final String? description;
  final String? imageUrl;
  final String? brewersTips;

  const Beer(
      {this.id,
      this.name,
      this.tagline,
      this.description,
      this.imageUrl,
      this.brewersTips});

  factory Beer.fromJson(Map<String, dynamic> json) => _$BeerFromJson(json);
}
