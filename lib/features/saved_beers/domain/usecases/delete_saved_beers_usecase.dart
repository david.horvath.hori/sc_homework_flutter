import 'package:sc_homework/core/usecase/usecase.dart';
import 'package:sc_homework/features/beers/domain/repositories/beers_local_repository.dart';

class DeleteSavedBeersUseCase implements UseCase<void, void> {
  final LocalStorageRepository _localStorageRepository;

  DeleteSavedBeersUseCase(this._localStorageRepository);

  @override
  Future<void> call({void params}) async {
    try {
      final box = await _localStorageRepository.openBox();
      _localStorageRepository.clear(box);
    } catch (e) {
      rethrow;
    }
  }
}
