import 'package:sc_homework/core/usecase/usecase.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';
import 'package:sc_homework/features/beers/domain/repositories/beers_local_repository.dart';

class GetSavedBeerUseCase implements UseCase<BeerEntity, int> {
  final LocalStorageRepository _localStorageRepository;

  GetSavedBeerUseCase(this._localStorageRepository);

  @override
  Future<BeerEntity> call({int? params}) async {
    try {
      final box = await _localStorageRepository.openBox();
      return _localStorageRepository.get(box, params!);
    } catch (e) {
      rethrow;
    }
  }
}
