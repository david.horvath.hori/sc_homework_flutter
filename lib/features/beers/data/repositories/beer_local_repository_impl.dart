import 'package:hive/hive.dart';
import 'package:sc_homework/core/constants/app_constants.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';
import 'package:sc_homework/features/beers/domain/repositories/beers_local_repository.dart';

class LocalStorageRepositoryImpl extends LocalStorageRepository {
  Type boxType = BeerEntity;

  @override
  Future<Box> openBox() async {
    final Box box = await Hive.openBox<BeerEntity>(AppConstants.featureBoxName);
    return box;
  }

  @override
  List<BeerEntity> getAll(Box box) {
    return box.values.toList() as List<BeerEntity>;
  }

  @override
  Future<void> add(Box box, BeerEntity beer) async {
    await box.put(beer.id, beer);
  }

  @override
  BeerEntity get(Box box, int id) {
    List<BeerEntity> beers = getAll(box);
    return beers.firstWhere((element) => element.id == id);
  }

  @override
  Future<void> remove(
    Box box,
    BeerEntity beer,
  ) async {
    await box.delete(beer.id);
  }

  @override
  Future<void> clear(Box box) async {
    await box.clear();
  }
}
