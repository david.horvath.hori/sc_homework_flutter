import 'package:equatable/equatable.dart';

sealed class SavedBeersEvent extends Equatable {
  const SavedBeersEvent();

  @override
  List<Object?> get props => [];
}

final class SavedBeersEventInitial extends SavedBeersEvent {
  const SavedBeersEventInitial();
}

final class SavedBeersEventFetch extends SavedBeersEvent {
  const SavedBeersEventFetch();
}

final class SavedBeersEventDelete extends SavedBeersEvent {
  const SavedBeersEventDelete();
}
