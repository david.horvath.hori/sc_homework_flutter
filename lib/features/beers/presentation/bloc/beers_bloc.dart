import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_homework/features/beers/domain/usecases/get_beer_usecase.dart';
import 'package:sc_homework/features/beers/domain/usecases/save_beer_usecase.dart';
import 'package:sc_homework/features/beers/presentation/bloc/beers_event.dart';
import 'package:sc_homework/features/beers/presentation/bloc/beers_state.dart';

class BeersBlock extends Bloc<BeersEvent, BeersState> {
  final GetBeerUseCase _getBeerUseCase;
  final SaveBeerUseCase _saveBeerUseCase;
  BeersBlock(
    this._getBeerUseCase,
    this._saveBeerUseCase,
  ) : super(const BeersStateDone(status: BeersStatus.initial, beer: null)) {
    on<BeersEventInitial>(_onInitial);
    on<BeersEventFetch>(_onFetchBeers);
    on<BeersEventSave>(_onSaveBeers);
  }

  void _onInitial(BeersEventInitial event, Emitter<BeersState> emit) async {
    emit(const BeersStateDone(status: BeersStatus.initial, beer: null));
  }

  void _onFetchBeers(BeersEventFetch event, Emitter<BeersState> emit) async {
    emit(const BeersStateLoading());
    final beer = await _getBeerUseCase();
    emit(BeersStateDone(
        status: BeersStatus.success, beer: beer, counter: event.counter + 1));
  }

  void _onSaveBeers(BeersEventSave event, Emitter<BeersState> emit) async {
    _saveBeerUseCase(params: event.beer);
    emit(const BeersStateLoading());
    final beer = await _getBeerUseCase();
    emit(BeersStateDone(
        status: BeersStatus.success, beer: beer, counter: event.counter + 1));
  }
}
