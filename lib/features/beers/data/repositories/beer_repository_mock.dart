import 'package:flutter/material.dart';
import 'package:sc_homework/core/constants/app_constants.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';
import 'package:sc_homework/features/beers/domain/repositories/beer_repository.dart';

class BeerRepositoryMock implements BeerRepository {
  const BeerRepositoryMock();

  @override
  Future<BeerEntity> getRandomBeer() async {
    try {
      final response = await Future.delayed(const Duration(seconds: 2), () {
        return AppConstants.mockedBeer;
      });
      return response;
    } catch (e) {
      debugPrint(e.toString());
      rethrow;
    }
  }
}
