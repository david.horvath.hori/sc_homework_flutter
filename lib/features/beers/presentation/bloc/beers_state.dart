import 'package:equatable/equatable.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';

enum BeersStatus { initial, loading, success, empty, error }

abstract class BeersState extends Equatable {
  final BeerEntity? beer;
  final int? counter;

  const BeersState({this.beer, this.counter});

  @override
  List<Object> get props => [beer!, counter!];
}

class BeersStateLoading extends BeersState {
  const BeersStateLoading();
}

class BeersStateDone extends BeersState {
  final BeersStatus status;

  const BeersStateDone({required this.status, BeerEntity? beer, int? counter})
      : super(beer: beer, counter: counter);
}
