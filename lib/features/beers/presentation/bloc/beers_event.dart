import 'package:equatable/equatable.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';

sealed class BeersEvent extends Equatable {
  const BeersEvent();

  @override
  List<Object?> get props => [];
}

final class BeersEventInitial extends BeersEvent {
  const BeersEventInitial();
}

final class BeersEventFetch extends BeersEvent {
  final int counter;
  const BeersEventFetch({required this.counter});
}

final class BeersEventSave extends BeersEvent {
  final int counter;
  final BeerEntity beer;
  const BeersEventSave({required this.beer, required this.counter});
}
