import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_homework/features/saved_beers/domain/usecases/delete_saved_beers_usecase.dart';
import 'package:sc_homework/features/saved_beers/domain/usecases/get_saved_beers_usecase.dart';
import 'package:sc_homework/features/saved_beers/presentation/bloc/saved_beers_event.dart';
import 'package:sc_homework/features/saved_beers/presentation/bloc/saved_beers_state.dart';

class SavedBeersBloc extends Bloc<SavedBeersEvent, SavedBeersState> {
  final GetSavedBeersUseCase _getSavedBeersUseCase;
  final DeleteSavedBeersUseCase _deleteSavedBeersUseCase;

  SavedBeersBloc(
    this._getSavedBeersUseCase,
    this._deleteSavedBeersUseCase,
  ) : super(const SavedBeerStateDone(
            status: SavedBeerEventStatus.initial, beers: [])) {
    on<SavedBeersEventInitial>(_onInitial);
    on<SavedBeersEventFetch>(_onFetchBeers);
    on<SavedBeersEventDelete>(_onDeleteBeers);
  }

  void _onInitial(
      SavedBeersEventInitial event, Emitter<SavedBeersState> emit) async {
    emit(const SavedBeerStateDone(
        status: SavedBeerEventStatus.initial, beers: []));
  }

  void _onFetchBeers(
      SavedBeersEventFetch event, Emitter<SavedBeersState> emit) async {
    emit(const SavedBeerStateLoading());
    final beers = await _getSavedBeersUseCase();
    emit(
        SavedBeerStateDone(status: SavedBeerEventStatus.success, beers: beers));
  }

  void _onDeleteBeers(
      SavedBeersEventDelete event, Emitter<SavedBeersState> emit) async {
    emit(const SavedBeerStateLoading());
    _deleteSavedBeersUseCase();
    emit(const SavedBeerStateDone(
        status: SavedBeerEventStatus.success, beers: []));
  }
}
