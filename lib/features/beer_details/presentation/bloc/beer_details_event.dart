import 'package:equatable/equatable.dart';

sealed class BeerDetailsEvent extends Equatable {
  const BeerDetailsEvent();

  @override
  List<Object?> get props => [];
}

final class BeerDetailsEventInitial extends BeerDetailsEvent {
  final int beerId;
  const BeerDetailsEventInitial({required this.beerId});

  @override
  List<Object?> get props => [beerId];
}
