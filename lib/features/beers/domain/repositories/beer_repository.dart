import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';

abstract class BeerRepository {
  Future<BeerEntity> getRandomBeer();
}
