import 'package:hive/hive.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';

abstract class LocalStorageRepository {
  Future<Box> openBox();
  List<BeerEntity> getAll(Box box);
  BeerEntity get(Box box, int id);
  Future<void> add(Box box, BeerEntity beer);
  Future<void> remove(Box box, BeerEntity beer);
  Future<void> clear(Box box);
}
