import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';
import 'package:sc_homework/features/beers/presentation/bloc/beers_bloc.dart';
import 'package:sc_homework/features/beers/presentation/bloc/beers_event.dart';

class BeerCard extends StatelessWidget {
  final BeerEntity? beer;

  const BeerCard({Key? key, this.beer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Card(
        elevation: 4.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ClipRRect(
              borderRadius:
                  const BorderRadius.vertical(top: Radius.circular(16.0)),
              child: beer?.imageUrl != null
                  ? Container(
                      height: 400.0,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.vertical(
                            top: Radius.circular(16.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.15),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: const Offset(2, 2),
                          ),
                        ],
                      ),
                      child: Image.network(
                        beer!.imageUrl!,
                        fit: BoxFit.fitHeight,
                      ),
                    )
                  : Container(
                      height: 400.0,
                      color: Colors.grey,
                      child: const Center(
                        child: Icon(
                          Icons.image,
                          size: 60.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    beer?.name ?? "",
                    style: const TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 4.0),
                  Text(
                    beer?.tagline ?? "",
                    style: const TextStyle(
                      fontSize: 14.0,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton.icon(
                  onPressed: () {
                    var _beersBloc = context.read<BeersBlock>();
                    _beersBloc.add(
                        BeersEventFetch(counter: _beersBloc.state.counter!));
                  },
                  icon: const Icon(Icons.thumb_down),
                  label: const Text("Nope"),
                ),
                ElevatedButton.icon(
                  onPressed: () {
                    if (beer != null) {
                      var _beersBloc = context.read<BeersBlock>();
                      _beersBloc.add(BeersEventSave(
                          beer: beer!, counter: _beersBloc.state.counter!));
                    }
                  },
                  icon: const Icon(Icons.thumb_up),
                  label: const Text("Yeah, I like it"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
