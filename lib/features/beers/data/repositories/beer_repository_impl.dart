import 'package:flutter/material.dart';
import 'package:sc_homework/features/beers/data/datasources/beer_api_client.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';
import 'package:sc_homework/features/beers/domain/repositories/beer_repository.dart';

class BeerRepositoryImpl implements BeerRepository {
  final ApiClient _apiClient;

  const BeerRepositoryImpl(
    this._apiClient,
  );

  @override
  Future<BeerEntity> getRandomBeer() async {
    try {
      final beerModel = await _apiClient.getRandomBeer();
      return BeerEntity.fromModel(beerModel);
    } catch (e) {
      debugPrint(e.toString());
      rethrow;
    }
  }
}
