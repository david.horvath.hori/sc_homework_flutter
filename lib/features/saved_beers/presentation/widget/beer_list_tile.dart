import 'package:flutter/material.dart';
import 'package:sc_homework/features/beer_details/presentation/page/beer_details_page.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';

class BeerListItem extends StatelessWidget {
  final BeerEntity beer;

  const BeerListItem({Key? key, required this.beer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: ListTile(
        contentPadding: const EdgeInsets.all(16.0),
        leading: ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: beer.imageUrl != null
              ? Image.network(
                  beer.imageUrl!,
                  width: 80.0,
                  height: 80.0,
                  fit: BoxFit.fitHeight,
                )
              : Container(
                  width: 80.0,
                  height: 80.0,
                  color: Colors.grey,
                  child: const Center(
                    child: Icon(
                      Icons.image,
                      size: 40.0,
                      color: Colors.white,
                    ),
                  ),
                ),
        ),
        title: Text(
          beer.name ?? "",
          style: const TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Text(
          beer.tagline ?? "",
          style: const TextStyle(
            fontSize: 14.0,
            color: Colors.grey,
          ),
        ),
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => BeerDetailsPage(
                beerId: beer.id!,
              ),
            ),
          );
        },
      ),
    );
  }
}
