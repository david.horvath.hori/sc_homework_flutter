import 'package:sc_homework/core/usecase/usecase.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';
import 'package:sc_homework/features/beers/domain/repositories/beers_local_repository.dart';

class GetSavedBeersUseCase implements UseCase<List<BeerEntity>, void> {
  final LocalStorageRepository _localStorageRepository;

  GetSavedBeersUseCase(this._localStorageRepository);

  @override
  Future<List<BeerEntity>> call({void params}) async {
    try {
      final box = await _localStorageRepository.openBox();
      return _localStorageRepository.getAll(box);
    } catch (e) {
      rethrow;
    }
  }
}
