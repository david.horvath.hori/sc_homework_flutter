import 'package:get_it/get_it.dart';
import 'package:sc_homework/features/beer_details/domain/usecases/get_saved_beer_usecase.dart';
import 'package:sc_homework/features/beer_details/presentation/bloc/beer_details_bloc.dart';
import 'package:sc_homework/features/beers/data/datasources/beer_api_client.dart';
import 'package:sc_homework/features/beers/data/repositories/beer_local_repository_impl.dart';
import 'package:sc_homework/features/beers/data/repositories/beer_repository_impl.dart';
import 'package:sc_homework/features/beers/data/repositories/beer_repository_mock.dart';
import 'package:sc_homework/features/beers/domain/repositories/beers_local_repository.dart';
import 'package:sc_homework/features/beers/domain/repositories/beer_repository.dart';
import 'package:sc_homework/features/beers/domain/usecases/get_beer_usecase.dart';
import 'package:sc_homework/features/beers/domain/usecases/save_beer_usecase.dart';
import 'package:sc_homework/features/beers/presentation/bloc/beers_bloc.dart';
import 'package:sc_homework/features/saved_beers/domain/usecases/delete_saved_beers_usecase.dart';
import 'package:sc_homework/features/saved_beers/domain/usecases/get_saved_beers_usecase.dart';
import 'package:sc_homework/features/saved_beers/presentation/bloc/saved_beers_bloc.dart';

final sl = GetIt.instance;

Future<void> initializeDependencies() async {
  // Repositories
  // sl.registerSingleton<BeerRepository>(const BeerRepositoryMock());
  sl.registerSingleton<BeerRepository>(BeerRepositoryImpl(ApiClient()));
  sl.registerSingleton<LocalStorageRepository>(LocalStorageRepositoryImpl());

  // UseCases
  sl.registerSingleton<GetBeerUseCase>(GetBeerUseCase(sl()));
  sl.registerSingleton<SaveBeerUseCase>(SaveBeerUseCase(sl()));
  sl.registerSingleton<GetSavedBeersUseCase>(GetSavedBeersUseCase(sl()));
  sl.registerSingleton<DeleteSavedBeersUseCase>(DeleteSavedBeersUseCase(sl()));
  sl.registerSingleton<GetSavedBeerUseCase>(GetSavedBeerUseCase(sl()));

  // Blocs
  sl.registerFactory<BeersBlock>(() => BeersBlock(sl(), sl()));
  sl.registerFactory<SavedBeersBloc>(() => SavedBeersBloc(sl(), sl()));
  sl.registerFactory<BeerDetailsBloc>(() => BeerDetailsBloc(sl()));
}
