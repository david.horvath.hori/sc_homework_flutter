import 'package:sc_homework/core/usecase/usecase.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';
import 'package:sc_homework/features/beers/domain/repositories/beers_local_repository.dart';

class SaveBeerUseCase implements UseCase<void, BeerEntity> {
  final LocalStorageRepository _localStorageRepository;

  SaveBeerUseCase(this._localStorageRepository);

  @override
  Future<void> call({BeerEntity? params}) async {
    try {
      final box = await _localStorageRepository.openBox();
      await _localStorageRepository.add(box, params!);
    } catch (e) {
      rethrow;
    }
  }
}
