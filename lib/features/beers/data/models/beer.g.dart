// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: implicit_dynamic_parameter

part of 'beer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Beer _$BeerFromJson(Map<String, dynamic> json) => $checkedCreate(
      'Beer',
      json,
      ($checkedConvert) {
        final val = Beer(
          id: $checkedConvert('id', (v) => v as int?),
          name: $checkedConvert('name', (v) => v as String?),
          tagline: $checkedConvert('tagline', (v) => v as String?),
          description: $checkedConvert('description', (v) => v as String?),
          imageUrl: $checkedConvert('image_url', (v) => v as String?),
          brewersTips: $checkedConvert('brewers_tips', (v) => v as String?),
        );
        return val;
      },
      fieldKeyMap: const {
        'imageUrl': 'image_url',
        'brewersTips': 'brewers_tips'
      },
    );
