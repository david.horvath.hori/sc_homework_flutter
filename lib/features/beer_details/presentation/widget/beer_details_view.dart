import 'package:flutter/material.dart';
import 'package:sc_homework/features/beers/domain/entities/beer_entity.dart';

class BeerDetails extends StatelessWidget {
  final BeerEntity beer;

  const BeerDetails({Key? key, required this.beer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 300.0,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.15),
                  spreadRadius: 1,
                  blurRadius: 2,
                  offset: const Offset(0, 10),
                ),
              ],
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16.0),
              child: beer.imageUrl != null
                  ? Image.network(
                      beer.imageUrl!,
                      width: double.infinity,
                      height: 200.0,
                      fit: BoxFit.fitHeight,
                    )
                  : Container(
                      width: double.infinity,
                      height: 200.0,
                      color: Colors.grey,
                      child: const Center(
                        child: Icon(
                          Icons.image,
                          size: 60.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
            ),
          ),
          const SizedBox(height: 16.0),
          Text(
            beer.name ?? '',
            style: const TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 8.0),
          Text(
            beer.tagline ?? '',
            style: const TextStyle(
              fontSize: 16.0,
              color: Colors.grey,
            ),
          ),
          const SizedBox(height: 16.0),
          const Text(
            'Description:',
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 8.0),
          Text(
            beer.description ?? '',
            style: const TextStyle(
              fontSize: 16.0,
            ),
          ),
          const SizedBox(height: 16.0),
          const Text(
            "Brewer's Tips:",
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 8.0),
          Text(
            beer.brewersTips ?? '',
            style: const TextStyle(
              fontSize: 16.0,
            ),
          ),
        ],
      ),
    );
  }
}
