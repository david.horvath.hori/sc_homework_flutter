import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:sc_homework/features/beers/data/models/beer.dart';

class RequestFailure implements Exception {}

class NotFoundFailure implements Exception {}

class ApiClient {
  final http.Client _httpClient;

  ApiClient({http.Client? httpClient})
      : _httpClient = httpClient ?? http.Client();

  static const _baseUrl = 'api.punkapi.com';
  // static const _baseUrl = 'biiohxvczthwzdpipzja.supabase.co';
  static const _partUrl = '/v2/beers/random';
  // static const _partUrl = '/rest/v1/Beer';

  static const Map<String, String> requestHeaders = {
    'apikey':
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImJpaW9oeHZjenRod3pkcGlwemphIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDEwODk1MTEsImV4cCI6MjAxNjY2NTUxMX0.WydXL2ryOajG9wvPFUhhVdrarg3_GW9aCdrY31lyJLE',
    'Authorization':
        'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImJpaW9oeHZjenRod3pkcGlwemphIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDEwODk1MTEsImV4cCI6MjAxNjY2NTUxMX0.WydXL2ryOajG9wvPFUhhVdrarg3_GW9aCdrY31lyJLE'
  };

  Future<Beer> getRandomBeer() async {
    final request = Uri.https(_baseUrl, _partUrl);

    final beersResponse = await _httpClient.get(request);
    // final beersResponse =
    //     await _httpClient.get(request, headers: requestHeaders);

    if (beersResponse.statusCode != 200) {
      throw RequestFailure();
    }

    final responseJson = jsonDecode(beersResponse.body) as List<dynamic>;

    return Beer.fromJson(responseJson.first);
  }
}
